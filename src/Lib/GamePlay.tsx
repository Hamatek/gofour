import * as _ from "lodash";

export const createBoard = (xLength: number = 7, yLength: number = 6) => {
  const board: number[][] = Array.from(Array(yLength)).map(e =>
    Array(xLength).fill(0)
  );

  return board;
};
export const findRowIndex = (
  actions: any,
  col: number,
  boardLength: number
) => {
  const selectedColumn = actions.filter(
    (action: any) => action.columnIndex === col
  );
  if (selectedColumn.length > 0) {
    const lastColumnAction = [...selectedColumn].pop();
    const decrementIndex = lastColumnAction.rowIndex - 1;
    return decrementIndex < 0 ? -1 : decrementIndex;
  } else {
    return boardLength - 1;
  }
};

export const checkActionsForWin = (
  actions: any,
  player: number,
  diagonalsKeys: any
) => {
  const playerActions = [...actions].filter(
    (action: any) => action.player === player
  );

  // check columns
  const columnWinner = reduceActions(
    6,
    "columnIndex",
    "rowIndex",
    playerActions
  );
  // check rows
  const rowWinner = reduceActions(5, "rowIndex", "columnIndex", playerActions);

  // check diag
  // TODO: bug with column or row returning true in some false case...
  const diagonalWinner = checkDiagonals(playerActions, diagonalsKeys);
  return rowWinner || columnWinner || diagonalWinner;
};

const returnKey = (column: number, row: number) => `${column}-${row}`;
const checkDiagonals = (actions: any, diagonalsKeys: any) => {
  const listOfActionsKey = [...actions]
    .reduce((array: any, action: any) => {
      array.push(action.key);
      return array;
    }, [])
    .sort();

  let winner = false;
  diagonalsKeys.forEach((winningKeys: any) => {
    const match = _.difference(winningKeys, listOfActionsKey).length === 0;
    if (match) {
      winner = true;
    }
  });

  return winner;
};

const reduceActions = (
  length: number,
  filterType: string,
  reduceType: string,
  actions: any
) => {
  let winner = false;
  // const filteredActions = [];

  for (let num = length; num >= 0; num--) {
    const filtered = [...actions]
      .filter((action: any) => action[filterType] === num)
      .reduce((array: any, action: any) => {
        array.push(action[reduceType]);
        return array.sort();
      }, []);

    winner = winner || checkArraySequence(filtered);
    // filteredActions.push(filtered);
    if (winner) {
      console.log("winner", winner);
      break;
    }
  }
  return winner;
};

const checkArraySequence = (array: any) => {
  let count = 1;
  let previous = -1;

  array.sort().forEach((element: any) => {
    // console.log("previous %i, element %i", previous + 1, element);
    if (element === previous + 1) {
      count++;
    } else {
      count = 1;
    }
    previous = element;
  });

  return count >= 4;
};
export const getAllDiagonalsKeys = () => {
  const numColumns = 7;
  const numRows = 6;
  const allKeys = [];
  for (let column = 0; column < 5; column++) {
    for (let row = 0; row < 7; row++) {
      if (row - 3 > -1 && column + 3 < numColumns) {
        allKeys.push([
          returnKey(column, row),
          returnKey(column + 1, row - 1),
          returnKey(column + 2, row - 2),
          returnKey(column + 3, row - 3)
        ]);
      }
      // if the bottom right contains possible win
      if (row + 3 < numRows && column + 3 < numColumns) {
        allKeys.push([
          returnKey(column, row),
          returnKey(column + 1, row + 1),
          returnKey(column + 2, row + 2),
          returnKey(column + 3, row + 3)
        ]);
      }
      // if the bottom left contains possible win
      if (row + 3 < numRows && column - 3 > -1) {
        allKeys.push([
          returnKey(column, row),
          returnKey(column - 1, row + 1),
          returnKey(column - 2, row + 2),
          returnKey(column - 3, row + 3)
        ]);
      }
      // if the top left contains a possible win
      if (row - 3 > -1 && column - 3 > -1) {
        allKeys.push([
          returnKey(column, row),
          returnKey(column - 1, row - 1),
          returnKey(column - 2, row - 2),
          returnKey(column - 3, row - 3)
        ]);
      }
    }
  }
  return allKeys;
};

export const changeTurn = (players: number[]) => players.reverse();

export default {
  createBoard,
  findRowIndex,
  checkActionsForWin,
  getAllDiagonalsKeys
};
