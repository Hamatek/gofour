import * as React from "react";
// import styled from "react-emotion";

// const Button = styled("button")`
//   display: inline-block;
//   position: relative;
//   padding: 20px 38px;
//   top: 0;
//   font-size: 30px;
//   font-family: "Open Sans", Helvetica;
//   border-radius: 4px;
//   border-bottom: 1px solid rgba(28, 227, 125, 0.5);
//   background: rgba(22, 230, 137, 1);
//   color: #fff;
//   box-shadow: 0px 0px 0px rgba(15, 165, 60, 0.1);
//   cursor: pointer;
//   width: 50%;
//   margin-top: 5%;
//   align-self: center;
// `;
export interface GameBtnProps {
  label: string;
  onClick: any;
}
// cant use emotion it swallow the click event no time to search why
export function GameBtn(props: GameBtnProps) {
  return (
    <button className="btn-game" onClick={props.onClick}>
      {props.label}
    </button>
  );
}
