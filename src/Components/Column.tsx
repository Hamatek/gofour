import * as React from "react";

export interface ColumnProps {
  columnIndex: number;
  addDisc: any;
  children: any;
}

export default class Column extends React.Component<ColumnProps, any> {
  public render() {
    const { columnIndex, children } = this.props;
    return (
      <div className={`column-${columnIndex}`} onClick={this.selectColumn}>
        {children}
      </div>
    );
  }
  public selectColumn = () => {
    this.props.addDisc(this.props.columnIndex);
  };
}
