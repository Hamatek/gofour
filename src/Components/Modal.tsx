import * as React from "react";
import { GameBtn } from "./GameBtn";

export interface ModalProps {
  winner: any;
  restart: any;
}

export default class Modal extends React.PureComponent<ModalProps, any> {
  public render() {
    const { winner, restart } = this.props;
    if (winner) {
      return (
        <div className="modal">
          <img
            src={`./assets/${winner.draw ? "draw" : "winner"}.png`}
            alt="winner icon"
          />
          <p className="win">{winner.draw ? "DRAW" : "VICTORY"}</p>
          <p>{winner.label} </p>
          <GameBtn label="Play Again" onClick={restart} />
        </div>
      );
    } else {
      return null;
    }
  }
}
