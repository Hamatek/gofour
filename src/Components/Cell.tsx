import * as React from "react";

export interface CellProps {
  player: number;
  children: any;
}

export function Cell(props: CellProps) {
  const { player } = props;
  return (
    <div className={`cell-`}>
      {/* {!!player && React.cloneElement(props.children, props)} */}
      {!!player && props.children}
    </div>
  );
}
