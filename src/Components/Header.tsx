import * as React from "react";

interface AppProps {
  title: string;
}

const Header: React.SFC<AppProps> = props => {
  return (
    <header className="App-header">
      <img src={"./logo.png"} className="App-logo" alt="logo" />
      <h1 className="App-title">{props.title}</h1>
    </header>
  );
};

export default Header;
