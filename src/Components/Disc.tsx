import * as React from "react";
import styled from "react-emotion";

const StyledDisc = styled("img")`
  user-select: none;
  width: 55px;
  height: 55px;
  -webkit-filter: drop-shadow(0px 0px 2px #ffeb3b);
`;

interface DiscProps {
  player: number;
  customStyle?: string;
}

const Disc: React.SFC<DiscProps> = props => {
  const { player, customStyle } = props;
  return <StyledDisc className={customStyle} src={`./assets/${player}.png`} />;
};

export default Disc;
