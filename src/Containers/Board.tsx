import * as React from "react";
import { connect } from "react-redux";
import BoardActions from "../Redux/BoardRedux";
import SelectedBoardActions from "../Selectors/boardActions";
import Disc from "../Components/Disc";
import { Cell } from "../Components/Cell";
import Column from "../Components/Column";
import Modal from "../Components/Modal";

export interface BoardProps {
  playDisc: any;
  restart: any;
  winner: any;
  board: number[][];
  players: number[];
}

class Board extends React.Component<BoardProps, any> {
  public render() {
    const { board, players, playDisc, restart, winner } = this.props;
    return (
      <div className="board">
        <Modal winner={winner} restart={restart} />
        {board[0].map((item, columnIndex) => (
          <Column
            key={`column-${columnIndex}`}
            columnIndex={columnIndex}
            addDisc={playDisc}
          >
            <Disc
              customStyle="drop-coin"
              player={players[0]}
              key={`col-disc-${columnIndex}`}
            />
            {board.map((row, rowIndex) => (
              <Cell
                player={row[columnIndex]}
                key={`cell-${columnIndex}-${rowIndex}`}
              >
                <Disc
                  player={row[columnIndex]}
                  key={`disc-${columnIndex}-${rowIndex}`}
                />
              </Cell>
            ))}
          </Column>
        ))}
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    board: SelectedBoardActions(state.game),
    players: state.game.players,
    winner: state.game.winner
  };
};
const mapDispatchToProps = (dispatch: any) => {
  return {
    playDisc: (columnIndex: number) => dispatch(BoardActions.play(columnIndex)),
    restart: () => dispatch(BoardActions.restart())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Board);
