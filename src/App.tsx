import * as React from "react";
import "./App.css";

import Header from "./Components/Header";
import Board from "./Containers/Board";

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <Header title="Welcome to Go four" />
        <Board />
      </div>
    );
  }
}

export default App;
