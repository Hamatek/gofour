import { createReducer, createActions } from "reduxsauce";
import * as Immutable from "seamless-immutable";
import { createBoard, getAllDiagonalsKeys } from "../Lib/GamePlay";
/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  addAction: ["lastAction"],
  play: ["columnIndex"],
  winner: ["winner"],
  changeTurn: null,
  restart: null
});

export const BoardTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  board: createBoard(),
  actions: [],
  diagonalsKeys: getAllDiagonalsKeys(),
  players: [1, 2],
  winner: false,
  rowLength: 6,
  columnLength: 7
});

/* ------------- Store Selectors ------------- */

export const BoardSelectors = {
  getGame: (state: any) => state.game,
  getActions: (state: any) => state.game.actions,
  getBoard: (state: any) => state.game.board
};

/* ------------- Reducers ------------- */

export const restart = (state: any) => INITIAL_STATE;

export const addAction = (state: any, { lastAction }: any) =>
  state.merge({ actions: state.actions.concat(lastAction) });

export const addWinner = (state: any, { winner }: any) =>
  state.merge({ winner });

export const changeTurn = (state: any) =>
  state.merge({ players: [...state.players].reverse() });

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CHANGE_TURN]: changeTurn,
  [Types.WINNER]: addWinner,
  [Types.RESTART]: restart,
  [Types.ADD_ACTION]: addAction
});
