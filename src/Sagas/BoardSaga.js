// tslint:disable:no-console
import { call, put, select } from "redux-saga/effects";
import { delay } from "redux-saga";

import BoardActions, { BoardSelectors } from "../Redux/BoardRedux";

import GamePlay from "../Lib/GamePlay";

export function* playerAction(action) {
  const { columnIndex } = action;
  const {
    players,
    board,
    actions,
    diagonalsKeys,
    rowLength,
    columnLength
  } = yield select(BoardSelectors.getGame);

  const availableRowIndex = yield call(
    GamePlay.findRowIndex,
    actions,
    columnIndex,
    board.length
  );
  // console.log("column %i selected", columnIndex);
  // console.log("availableRowIndex", availableRowIndex);

  if (availableRowIndex < 0) {
    throw console.error("nono no you cant do that");
  } else {
    const newAction = {
      columnIndex,
      player: players[0],
      rowIndex: availableRowIndex,
      key: columnIndex + "-" + availableRowIndex
    };
    yield put(BoardActions.addAction(newAction));
    const allActions = actions.concat(newAction);
    const winner = yield call(
      GamePlay.checkActionsForWin,
      allActions,
      players[0],
      diagonalsKeys
    );
    if (winner) {
      yield put(BoardActions.winner({ label: `Player ${players[0]}` }));
    } else if (allActions.length === columnLength * rowLength) {
      yield put(BoardActions.winner({ draw: true, label: "NO LUCK" }));
    } else {
      yield put(BoardActions.changeTurn());
    }
  }
}
