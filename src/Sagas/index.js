import { takeLatest, takeEvery, all } from "redux-saga/effects";

/* ------------- Types ------------- */
import { BoardTypes } from "../Redux/BoardRedux";

/* ------------- Sagas ------------- */
import { playerAction } from "./BoardSaga";

export default function* root() {
  yield all([
    // some sagas only receive an action
    takeEvery(BoardTypes.PLAY, playerAction)
  ]);
}
