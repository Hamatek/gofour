import { createSelector } from "reselect";

const boardSelector = state => state.board;
const actionsSelector = state => state.actions;

const setBoardActions = (board, actions) => {
  // only work on data sets which have primitives as values (String, Numbers, Objects)
  const boardResult = JSON.parse(JSON.stringify(board));
  
  actions.forEach(element => {
    boardResult[element.rowIndex][element.columnIndex] = element.player;
  });
  // console.log("boardClone", boardResult);
  return boardResult;
};

export default createSelector(boardSelector, actionsSelector, setBoardActions);
