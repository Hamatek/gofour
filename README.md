Connect 4 

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


# UI Reseach
[Pinterest board]
You will see my amazing drawing skill on a big whiteboard.
(https://pin.it/ejn63lxyzehmqi)

# Project Management/ todos
[My Todos]
(https://workflowy.com/s/FiVv.2vEq9by2It)

First idea was to use a 2D array for everything, and then.... I rewrote everything to use a reselect approach with a board grid and a list of actions combined together using reselect.

But it made the winning condition a bit more challenging to cod. I found some interesting solutions that you will probably not see somewhere else ...


* TypeScript
It was my first time using Typescript and I did struggle with it, I didn't have the time to look into it and learn everything it has to offer and I end up using a lot of `:any` types due to my lack of knowledge and time to write all the proper interfaces types..
for the redux store, object, arrays ...

I also struggle with the Tslint rules and end up switching many off such as 
`"object-literal-sort-keys","no-console","ordered-imports"`

Regarding the TSconfig i had to add `"downlevelIteration": true` to make this piece of code work or replace spread operator `...` by `Array.from` 
``` const board: number[][] = Array.from(Array(yLength)).map(e =>
    Array(xLength).fill(0)
  );```